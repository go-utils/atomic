package atomic_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/go-utils/atomic"
)

func TestQueueEmpty(t *testing.T) {
	q := new(atomic.Queue)

	require.Equal(t, nil, q.Pop())
	require.Zero(t, q.Size())
}

func TestQueuePushPop1(t *testing.T) {
	q := new(atomic.Queue)

	q.Push(1)
	require.Equal(t, int32(1), q.Size())
	require.Equal(t, 1, q.Pop())
	require.Zero(t, q.Size())
	require.Nil(t, q.Pop())
}

func TestQueuePushPop2(t *testing.T) {
	q := new(atomic.Queue)

	q.Push(1)
	require.Equal(t, int32(1), q.Size())
	require.Equal(t, 1, q.Pop())
	require.Zero(t, q.Size())

	q.Push(2)
	require.Equal(t, int32(1), q.Size())
	require.Equal(t, 2, q.Pop())
	require.Zero(t, q.Size())
	require.Nil(t, q.Pop())
}

func TestQueuePushPop3(t *testing.T) {
	q := new(atomic.Queue)

	q.Push(1)
	q.Push(2)
	require.Equal(t, int32(2), q.Size())

	require.Equal(t, 1, q.Pop())
	require.Equal(t, int32(1), q.Size())
	require.Equal(t, 2, q.Pop())
	require.Zero(t, q.Size())
	require.Nil(t, q.Pop())
}

func BenchmarkPush(b *testing.B) {
	q := new(atomic.Queue)

	for i := 0; i < b.N; i++ {
		q.Push(i)
	}
	require.Equal(b, int32(b.N), q.Size())
}

func BenchmarkPop(b *testing.B) {
	q := new(atomic.Queue)

	for i := 0; i < b.N; i++ {
		q.Push(i)
	}
	require.Equal(b, int32(b.N), q.Size())

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		q.Pop()
	}
	require.Zero(b, q.Size())
}

func BenchmarkPushPop(b *testing.B) {
	q := new(atomic.Queue)

	for i := 0; i < b.N; i++ {
		q.Push(i)
		q.Pop()
	}
	require.Zero(b, q.Size())
}
