package atomic

import (
	"sync/atomic"
	"unsafe"
)

func (l *Queue) ptr() *unsafe.Pointer   { return (*unsafe.Pointer)(unsafe.Pointer(&l.state)) }
func (n *queueNode) u() unsafe.Pointer  { return unsafe.Pointer(n) }
func (s *queueState) u() unsafe.Pointer { return unsafe.Pointer(s) }
func (l *Queue) load() *queueState      { return (*queueState)(atomic.LoadPointer(l.ptr())) }

// func (n *queueNode) ptr() *unsafe.Pointer { return (*unsafe.Pointer)(unsafe.Pointer(&n.next)) }
// func (n *queueNode) load() *queueNode     { return (*queueNode)(atomic.LoadPointer(n.ptr())) }

func (l *Queue) swap(old, new *queueState) bool {
	return atomic.CompareAndSwapPointer(l.ptr(), old.u(), new.u())
}

// Queue is a first-in, first-out queue of values
type Queue struct {
	len   int32
	state *queueState
}

// Size returns the size of the queue
func (l *Queue) Size() int32 {
	if l == nil {
		return 0
	}
	return atomic.LoadInt32(&l.len)
}

// Push pushes a value onto the back of the queue
func (l *Queue) Push(v interface{}) {
	_ = *l // check for nil

	node := &queueNode{value: v}
	new := &queueState{back: node}

	for {
		old := l.load()

		if old == nil || old.front == nil {
			new.front = node
		} else {
			new.front = old.front
		}

		if !l.swap(old, new) {
			continue
		}

		if old != nil && old.back != nil {
			old.back.prev = node
		}
		atomic.AddInt32(&l.len, 1)
		return
	}
}

// Pop pops a value off the front of the queue or returns nil
func (l *Queue) Pop() (interface{}, bool) {
	if l == nil {
		return nil, false
	}

	for {
		old := l.load()
		if old == nil || old.front == nil {
			return nil, false
		}

		new := &queueState{}
		if old.back != old.front {
			new.back = old.back
			new.front = old.front.prev
		}

		if !l.swap(old, new) {
			continue
		}

		atomic.AddInt32(&l.len, -1)
		return old.front.value, true
	}
}

type queueNode struct {
	prev  *queueNode
	value interface{}
}

type queueState struct {
	back, front *queueNode
}
