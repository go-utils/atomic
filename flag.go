package atomic

import "sync/atomic"

// Flag is an atomic boolean flag
type Flag int32

// Set attempts to set the flag
func (f *Flag) Set() bool {
	return atomic.CompareAndSwapInt32((*int32)(f), 0, 1)
}

// Clear attempts to clear the flag
func (f *Flag) Clear() bool {
	return atomic.CompareAndSwapInt32((*int32)(f), 1, 0)
}

// Value returns the value of the flag
func (f *Flag) Value() bool {
	return *f == 1
}
